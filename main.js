var gm = require('gm').subClass({imageMagick: true})
var smartcrop = require('smartcrop-gm')
var fs = require('mz/fs')
const express = require('express')
const app = express()

const port = process.env.IMAGE_PORT
const srcDir = process.env.IMAGE_SOURCE_PATH
const destDir = process.env.IMAGE_DEST_PATH

if (!port || !srcDir || !destDir) {
  console.log('Please set port, srcDir and destDir')
  process.exit(1)
}

console.log(`Start Service on port ${port}, dest ${destDir}, src ${srcDir}`)

function cropData (path, w, h) {
  return smartcrop.crop(path, {width: w, height: h}).then(result => {
    var crop = result.topCrop
    return crop
  })
}

app.get('/pics/:name', (request, response) => {
  let srcFile = srcDir + '/' + request.params.name
  response.sendFile(srcFile, {}, (err) => {
    if (err) {
      console.log(err)
    }
  })
})

app.get('/:w/:h/:file', (request, response) => {
  let w = request.params.w
  let h = request.params.h
  let file = request.params.file
  let srcFile = srcDir + '/' + request.params.file
  let destFile = destDir + '/' + w + 'x' + h + '_' + file

  console.log('dest', destFile)
  console.log('src', srcFile)

  fs.readFile(destFile).then(data => {
    console.log('serving file', destFile)
    response.sendFile(destFile)
  }).catch(() => {
    cropData(srcFile, w, h).then(cropData => {
      fs.readFile(srcFile).then(data => {
        gm(data)
          .crop(cropData.width, cropData.height, cropData.x, cropData.y)
          .resize(w, h)
          .write(destFile, error => {
            if (error) {}
            response.sendFile(destFile)
          })
      }, data => {
        response.status(404).send('File doesn\'t exist')
      })
    }, data => {
      response.status(404).send('File doesn\'t exist')
    })
  })
})

app.listen(port, (err) => {
  if (err) {
    return console.log('something bad happened', err)
  }
})
