FROM node:9.4.0 as builder
WORKDIR /usr/src/image-resize
ADD . /usr/src/image-resize/
RUN rm -rf node_modules && yarn install --only=production